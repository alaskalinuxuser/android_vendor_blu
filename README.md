# android_vendor_blu

Blu Life One X2 (BLOX2) vendor blobs. Https://thealaskalinuxuser.wordpress.com

Marshmallow - Under developement! WIP by the AlaskaLinuxUser

As of 08/17/2018 Build - Marshmallow
What works:
+ USB/MTP/PTP/ADB
+ Sound
+ Graphics
+ Data - LTE/3G
+ Dual Sim
+ Make/receive phone calls
+ GPS
+ WiFi
+ Hotspot

What sort of works:
+ Bluetooth - connects, but sometimes doesn't work right.

What doesn't work at all:
+ Fingerprint
+ Camera
